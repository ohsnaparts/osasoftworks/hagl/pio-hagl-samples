[CmdletBinding()]
param (
    [Parameter()]
    [ValidateSet('native-libgd', 'native-softraster')]
    [string] $Environment = 'native-libgd'
)

$OutputLogFile = Join-Path $PSScriptRoot './hagl.txt'

# initialize submodules in ./lib folder
git submodule init && git submodule update
# initialize submodules libraries in ./lib
git submodule foreach 'git submodule init && git submodule update'

./invoke-pio.ps1 run --environment $Environment
& "./.pio/build/$Environment/program" | Tee-Object -FilePath $OutputLogFile
