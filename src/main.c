#include "drawing_util.h"
#include "hagl.h"
#include "font6x9.h"
#include "color_util.h"
#include "stdlib.h"
#include "time.h"

#define DISPLAY_CENTER_X DISPLAY_WIDTH / 2
#define DISPLAY_CENTER_Y DISPLAY_HEIGHT / 2

#define NOISY_BOX_POS_X DISPLAY_CENTER_X
#define NOISY_BOX_POS_Y DISPLAY_CENTER_Y
#define NOISY_BOX_SIZE 200

#define BLACK_BOX_POS_X NOISY_BOX_POS_X
#define BLACK_BOX_POS_Y NOISY_BOX_POS_Y
#define BLACK_BOX_SIZE NOISY_BOX_SIZE + 200

static inline void initialize() {
    drawing_utils__initialize(time(NULL) * 1);
    color_utils__initialize(time(NULL) * 2);
    hagl_init();
}

int main()
{
    initialize();

    // The letter counts have been found by trying out what looks nice
    draw_random_letters(DISPLAY_WIDTH * 50, font6x9);

    fill_box_center_origin(
        BLACK_BOX_POS_X,
        BLACK_BOX_POS_Y,
        BLACK_BOX_SIZE,
        RGB565_BLACK);

    draw_random_letters(DISPLAY_WIDTH * 3, font6x9);

    draw_noisy_box_center_origin(
        NOISY_BOX_POS_X,
        NOISY_BOX_POS_Y,
        NOISY_BOX_SIZE);

    hagl_flush();
    hagl_close();
    return 0;
}
