
enum jpg_quality_level {
    /**
     * If quality is negative, the default IJG JPEG quality value (which should yield
     * a good general quality / size tradeoff for most situations) is used.
     */
    JPG_QUALITY_LEVEL__DEFAULT = -1,
    /**
     * Extreme loss of color and detail; the leaves are nearly unrecognizable.
     */
    JPG_QUALITY_LEVEL__MIN = 1,
    /**
     * Severe high frequency loss leads to obvious artifacts on subimage boundaries ("macroblocking")
     */
    JPG_QUALITY_LEVEL__LOW = 10,
    /**
     * Stronger artifacts; loss of high frequency information
     */
    JPG_QUALITY_LEVEL__MEDIUM = 25,
    /**
     * Initial signs of subimage artifacts
     */
    JPG_QUALITY_LEVEL__HIGH = 50,
    /**
     * Extremely minor artifacts
     */
    JPG_QUALITY_LEVEL__MAX = 100
};