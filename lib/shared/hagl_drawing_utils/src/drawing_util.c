#include "hagl.h"
#include "color_util.h"
#include "stdlib.h"


void drawing_utils__initialize(unsigned int randomizer_seed) {
    srand(randomizer_seed);
}


void draw_noisy_box_top_left_origin(
    uint16_t startX,
    uint16_t startY,
    uint16_t size)
{
    const int endX = startX + size;
    const int endY = startY + size;
    for (int x = startX; x < endX; x++)
    {
        for (int y = startY; y < endY; y++)
        {
            hagl_put_pixel(x, y, RANDOM_RGB565_COLOR);
        }
    }
}

void draw_noisy_box_center_origin(
    uint16_t center_origin_position_x,
    uint16_t center_origin_position_y,
    uint16_t size)
{
    const int offset_to_top_left_origin = size / 2;
    const int position_x = center_origin_position_x - offset_to_top_left_origin;
    const int position_y = center_origin_position_y - offset_to_top_left_origin;
    draw_noisy_box_top_left_origin(position_x, position_y, size);
}

void fill_box_center_origin(
    uint16_t center_origin_position_x,
    uint16_t center_origin_position_y,
    uint16_t size,
    uint32_t color)
{
    const int offset_to_top_left_origin = size / 2;
    hagl_fill_rectangle(
        center_origin_position_x - offset_to_top_left_origin,
        center_origin_position_y - offset_to_top_left_origin,
        center_origin_position_x + offset_to_top_left_origin,
        center_origin_position_y + offset_to_top_left_origin,
        color);
}

void draw_random_letters(uint32_t count, const unsigned char *font)
{
    for (uint32_t x = 0; x < count; x++)
    {
        const int x = rand() % DISPLAY_WIDTH;
        const int y = rand() % DISPLAY_HEIGHT;
        const unsigned char character = RANDOM_RGB565_COLOR;
        const int color = RANDOM_RGB565_COLOR;
        hagl_put_char(character, x, y, color, font);
    }
}