#pragma once
#include "stdint.h"

/**
 * @brief initializes the drawing utils
 *
 * @param randomizer_seed without seeding, rand() would return always
 *                        the same string of values.
 */
void drawing_utils__initialize(unsigned int randomizer_seed);

/**
 * @brief Draws a box filled with random pixel noise
 * @details For positioning, the origin is assumed to be top-left box corner
 *
 * @param top_left_origin_position_x the x coordinates of the upper right box corner
 * @param top_left_origin_position_y the y coordinates of the upper right box corner
 * @param size the width / height of the box
 */
void draw_noisy_box_top_left_origin(
    uint16_t top_left_origin_position_x,
    uint16_t top_left_origin_position_y,
    uint16_t size);

/**
 * @brief Draws a box filled with random pixel noise
 * @details For positioning, the origin is assumed to be the box center
 *
 * @param center_origin_position_x the x coordinate of the box center
 * @param center_origin_position_y the y coordinate of the box center
 * @param size the width / height of the box
 */
void draw_noisy_box_center_origin(
    uint16_t center_origin_position_x,
    uint16_t center_origin_position_y,
    uint16_t size);

/**
 * @brief Draws a filled square
 * @details For positioning, the origin is assumed to be the box center
 *
 * @param center_origin_position_x the x coordinate of the box center
 * @param center_origin_position_y the y coordinate of the box center
 * @param size the height / width of the box
 * @param color the rgb565 background color
 */
void fill_box_center_origin(
    uint16_t center_origin_position_x,
    uint16_t center_origin_position_y,
    uint16_t size,
    uint32_t color);

/**
 * @brief Draws random characters at random positions
 *
 * @param count The character count to be drawn
 * @param font The character font
 */
void draw_random_letters(
    uint32_t count,
    const unsigned char *font);