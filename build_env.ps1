Param (
    [Parameter(Mandatory)]
    [ValidateNotNullOrEmpty()]
    [string] $Environment,
    # ------------------------
    [Parameter(Mandatory)]
    [ValidateNotNullOrEmpty()]
    [string] $OutputExecutablePath
)

# build
./invoke-pio.ps1 run --environment "$environment"

# deploy
$ProjectRootPath = $PSScriptRoot
$BuildPath = Join-Path -Resolve $ProjectRootPath '.pio/build/$TargetEnvironment'
$ExecutablePath = Join-Path -Resolve $BuildPath 'program'
Copy-Item -Path $ExecutablePath -Destination $OutputExecutablePath