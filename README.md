# Libgd Canvas

A playground for experimenting with C canvas drawings.
This program will render canvas drawings into files on disk.

## Dependencies

* [PlatformIO]
* `apt install libgd-dev`

## Usage

```powershell
pwsh ./run.ps1 -Environment native-softraster
pwsh ./run.ps1 -Environment native-libgd
```

This creates a file called `hagl.bmp`/`hagl.txt` into the project root directory.

![image showing a sample of the image being created by the application][libgd-example-screenshot]
![image showing a sample of the textfile produced by the native-softraster application][softraster-example-screenshot]

[platformio]: https://docs.platformio.org/en/latest/
[libgd-example-screenshot]: ./screenshots/native-libgd-example.jpg
[softraster-example-screenshot]: ./screenshots/native-softraster-example.jpg